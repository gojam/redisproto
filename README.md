# Redis Protocol Writer

Convert a file of Redis commands into the Redis protocol.  

    LPUSH my_list one

becomes

    *3<CR><LF>
    $5<CR><LF>
    LPUSH<CR><LF>
    $7<CR><LF>
    my_list<CR><LF>
    $3<CR><LF>
    one<CR><LF>

## Usage

    redisproto file.txt
    cat file.txt | redisproto

## Example

To run a batch of commands on Redis:

    $ cat input.txt | redisproto | redis-cli --pipe


## Install

Checkout the code and simply:

    go install
