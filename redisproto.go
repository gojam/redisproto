package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {

	if len(os.Args) == 1 {
		info, err := os.Stdin.Stat()
		if err != nil {
			panic(err)
		}

		if info.Mode()&os.ModeCharDevice != 0 || info.Size() <= 0 {
			fmt.Println("The command works with pipes or filenames.")
			fmt.Println("Usage: cat file | redisproto")
			fmt.Println("Usage: redisproto file")
			return
		}

		parse(bufio.NewReader(os.Stdin))
		return
	}

	for _, fn := range os.Args[1:] {
		if reader, err := os.Open(fn); err == nil {
			parse(reader)
		}
	}
}

func parse(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	scanner.Split(bufio.ScanLines)

	out := strings.Builder{}
	eol := "\r\n"
	for scanner.Scan() {
		line := scanner.Text()
		if strings.TrimSpace(line) == "" {
			continue
		}

		// 1) number of arguments
		// For "SET foo bar", this is 3:
		//     *3<CR><LF>
		args := strings.Split(line, " ")
		cnt := len(args)
		out.WriteRune('*')
		out.WriteString(strconv.Itoa(cnt))
		out.WriteString(eol)

		// For each argument, print length of string then string
		//     $3<CR><LF>
		//     foo<BR><LF>
		//     ...
		for _, arg := range args {
			out.WriteRune('$')
			out.WriteString(strconv.Itoa(len(arg)))
			out.WriteString(eol)
			out.WriteString(arg)
			out.WriteString(eol)
		}
	}

	// output Redis protocol for one line of the input file
	fmt.Print(out.String())
}
